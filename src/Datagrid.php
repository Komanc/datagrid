<?php

/**
 * @license    MIT
 */

namespace Komanc\Datagrid;


class Datagrid extends \Nextras\Datagrid\Datagrid
{
    private $prerendered = FALSE;

    public function preRender()
    {
        if ($this->filterFormFactory) {
            $this['form']['filter']->setDefaults($this->filter);
        }

        $this->template->form                     = $this['form'];
        $this->template->data                     = $this->getData();
        $this->template->columns                  = $this->columns;
        $this->template->editRowKey               = $this->editRowKey;
        $this->template->rowPrimaryKey            = $this->rowPrimaryKey;
        $this->template->paginator                = $this->paginator;
        $this->template->sendOnlyRowParentSnippet = $this->sendOnlyRowParentSnippet;
        $this->template->cellsTemplates           = $this->getCellsTemplates();
        $this->template->showFilterCancel         = $this->filterDataSource != $this->filterDefaults; // @ intentionaly
        $this->template->setFile(__DIR__ . '/Datagrid.latte');

        $this->prerendered = TRUE;
    }

    public function render()
    {
        if (!$this->prerendered) {
            $this->preRender();
        }

        $this->onRender($this);
        $this->template->render();
    }
}
